// create an array of car objects
// create getter and setter for every field
// make, model, vrn, engine size, transmission (auto or manual), color, the num of previous owners, age of car.
// create an array of three car objects

const cars = [{
    vrn: "CE122TX",
    make: "Audi",
    engineSize: 3000,
    engineType: "v6",
    model: "Q8",
    color: "Blue",
    age: 2
},{
    vrn: "CE433YZ",
    make: "Mercedez",
    engineSize: 2500,
    engineType: "v6",
    model: "Tx",
    color: "Black",
    age: 1
}]

function getCarAge(idx) {
    return cars[idx].age
}

function setCarAge(idx, age) {
    cars[idx].age = age
}

function getCarColor(idx) {
    return cars[idx].color
}

function setCarColor(idx, color) {
    cars[idx].color = color
}


function getCarModel(idx) {
    return cars[idx].model
}

function setCarModel(idx, model) {
    cars[idx].model = model
}

function getCarEngineType(idx) {
    return cars[idx].engineType
}

function setCarEngineType(idx, EngineType) {
    cars[idx].engineType = engineType
}

function getCarEngineSize(idx) {
    return cars[idx].engineSize
}

function setCarEngineSize(idx, engineSize) {
    cars[idx].engineSize = engineSize
}

function getCarMake(idx) {
    return cars[idx].make
}

function setCarMake(idx, make) {
    cars[idx].make = make
}

function getCarVrn(idx){
    return cars[idx].vrn
}

function setCarVrn(idx, vrn) {
    cars[idx].vrn = vrn
}


console.log(`The car age is: ${getCarAge(1)} and the car color is: ${getCarColor(1)}`)